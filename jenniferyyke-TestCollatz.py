#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_1(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 999999)

    def test_read_2(self):
        s = "456789 34\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 456789)
        self.assertEqual(j, 34)

    def test_read_3(self):
        s = "14708 114709\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 14708)
        self.assertEqual(j, 114709)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(999998, 999999)
        self.assertEqual(v, 259)

    def test_eval_6(self):
        v = collatz_eval(88889, 56)
        self.assertEqual(v, 351)

    def test_eval_7(self):
        v = collatz_eval(90000, 99999)
        self.assertEqual(v, 333)

    def test_eval_8(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 999998, 999999, 259)
        self.assertEqual(w.getvalue(), "999998 999999 259\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 2389, 8973, 262)
        self.assertEqual(w.getvalue(), "2389 8973 262\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 5000, 99000, 351)
        self.assertEqual(w.getvalue(), "5000 99000 351\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_1(self):
        r = StringIO("10 100\n300 399\n1001 2000\n3000 3999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 100 119\n300 399 144\n1001 2000 182\n3000 3999 238\n")

    def test_solve_2(self):
        r = StringIO("4000 4010\n4011 5000\n6000 6999\n7000 7500\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "4000 4010 158\n4011 5000 215\n6000 6999 262\n7000 7500 239\n")

    def test_solve_3(self):
        r = StringIO("7501 8000\n8001 8010\n8011 9000\n9000 10000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "7501 8000 252\n8001 8010 146\n8011 9000 247\n9000 10000 260\n")
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
